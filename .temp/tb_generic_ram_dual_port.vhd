-------------------------------------------------------------------------------
-- Description:
-- Testbench for generic_ram_dual_port
--
-- Generic : 
-- M is the number of features (dimentions) in each vector
-- N is the number of bits for each vector (precision)
-- ClassSize is the number of bits to encode the class of the example
-- IDSize is the number of bits to encode the ID of the examples
--
-- ex : vector = [ft1 ft2 ... ftm]
--      feature = bit1 bit2 ... bitn
--
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY tb_generic_ram_dual_port IS
END ENTITY;

ARCHITECTURE RTL OF tb_generic_ram_dual_port IS

    -- Clock period definitions
    CONSTANT clock_period : TIME := 20 ns;

    --wait duration
    CONSTANT PERIOD : TIME := 0.02 us;

    CONSTANT data_width : INTEGER := 8; --word size
    CONSTANT addr_width : INTEGER := 5; --word count (2**addr_width)
    CONSTANT init_file : STRING := "../src/tesst.mif"; --file path ()

    SIGNAL CLK : STD_LOGIC;
    SIGNAL RST : STD_LOGIC;
    SIGNAL ENDSIM : STD_LOGIC; --simulation end signal

    SIGNAL data : STD_LOGIC_VECTOR(data_width - 1 DOWNTO 0);
    SIGNAL rdaddress : STD_LOGIC_VECTOR(addr_width - 1 DOWNTO 0);
    SIGNAL wraddress : STD_LOGIC_VECTOR(addr_width - 1 DOWNTO 0);
    SIGNAL wren : STD_LOGIC;
    SIGNAL q : STD_LOGIC_VECTOR(data_width - 1 DOWNTO 0);

BEGIN
    generic_ram_dual_port : ENTITY work.generic_ram_dual_port
        GENERIC MAP(
            data_width => data_width,
            addr_width => addr_width,
            init_file => init_file
        )
        PORT MAP(
            clock => CLK,
            data => data,
            rdaddress => rdaddress,
            wraddress => wraddress,
            wren => wren,
            q => q
        );

    -- clock process
    clock_process : PROCESS
    BEGIN
        IF (ENDSIM = '1') THEN
            WAIT;
        ELSE
            CLK <= '0';
            WAIT FOR clock_period/2;
            CLK <= '1';
            WAIT FOR clock_period/2;
        END IF;
    END PROCESS;
    
    -- test process
    Test : PROCESS
    BEGIN
        --simulation init start
        RST <= '1';
        ENDSIM <= '0';
        WAIT FOR PERIOD;
        RST <= '0';
        --simulation init end

        --test here
        rdaddress <= "00000";
        wraddress <= "00000";

        data <= "00000001";
        wren <= '0'; --disable writing
        WAIT FOR PERIOD;

        FOR i IN 0 TO 2 ** addr_width LOOP
            rdaddress <= STD_LOGIC_VECTOR(unsigned(rdaddress) + 1);
            WAIT FOR PERIOD;
        END LOOP;

        WAIT FOR PERIOD;

        --simulation end
        ENDSIM <= '1';
        WAIT;
    END PROCESS;
END ARCHITECTURE;