puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/generic_ram_dual_port.vhd
vcom -93 ../simu/tb_generic_ram_dual_port.vhd

vsim generic_ram_dual_port

add wave *

run -all

#cosmetic
wave zoom full