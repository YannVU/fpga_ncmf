-------------------------------------------------------------------------------
-- Description:
-- Testbench for node_splitter
--
--
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY tb_node_splitter IS
END ENTITY;

ARCHITECTURE RTL OF tb_node_splitter IS

    -- Clock period definitions
    CONSTANT clock_period : TIME := 20 ns;

    --wait duration
    CONSTANT PERIOD : TIME := 0.02 us;

    SIGNAL CLK : STD_LOGIC;
    SIGNAL RST : STD_LOGIC;
    SIGNAL ENDSIM : STD_LOGIC; --simulation end signal

    CONSTANT address_bitwidth : INTEGER := 6;
    CONSTANT feature_bitwidth : INTEGER := 8; --feature precision
    CONSTANT subfeature_count : INTEGER := 2; --number of features inside a node
    CONSTANT feature_count : INTEGER := 4; --number of features in the dataset

    SIGNAL node_info : STD_LOGIC_VECTOR (0 TO (address_bitwidth * 2) + (feature_bitwidth * subfeature_count * 2) + feature_count - 1 );
    SIGNAL left_addr : STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
    SIGNAL right_addr : STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
    SIGNAL centroid : STD_LOGIC_VECTOR ((feature_bitwidth * subfeature_count * 2) - 1 DOWNTO 0);
    SIGNAL subfeature_sel : STD_LOGIC_VECTOR (feature_count - 1 DOWNTO 0);

BEGIN
    node_splitter : ENTITY work.node_splitter
        GENERIC MAP(
            address_bitwidth => address_bitwidth,
            feature_bitwidth => feature_bitwidth,
            subfeature_count => subfeature_count,
            feature_count => feature_count
        )
        PORT MAP(
            i_node_info => node_info,
            o_left_addr => left_addr,
            o_right_addr => right_addr,
            o_centroid => centroid,
            o_subfeature_sel => subfeature_sel
        );

    -- clock process
    clock_process : PROCESS
    BEGIN
        IF (ENDSIM = '1') THEN
            WAIT;
        ELSE
            CLK <= '0';
            WAIT FOR clock_period/2;
            CLK <= '1';
            WAIT FOR clock_period/2;
        END IF;
    END PROCESS;

    -- test process
    Test : PROCESS
    BEGIN
        --simulation init start
        RST <= '1';
        ENDSIM <= '0';
        WAIT FOR PERIOD;
        RST <= '0';
        --simulation init end

        --test here
        node_info <= "000000" & "000000" & "00000000000000000000000000000000" & "0000";
        WAIT FOR PERIOD;
        node_info <= "100001" & "011110" & "10000000000000000000000000000001" & "0110";
        --simulation end
        ENDSIM <= '1';
        WAIT;
    END PROCESS;
END ARCHITECTURE;