puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/pointer_sel.vhd
vcom -93 ../simu/tb_pointer_sel.vhd

vsim tb_pointer_sel

add wave -noupdate -expand -group Simulation -label CLK /tb_pointer_sel/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_pointer_sel/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_pointer_sel/ENDSIM

add wave -noupdate -expand -group Constants -label address_bitwidth /tb_pointer_sel/address_bitwidth

add wave -noupdate -expand -group Signals -label selector /tb_pointer_sel/selector
add wave -noupdate -expand -group Signals -label left_addr /tb_pointer_sel/left_addr
add wave -noupdate -expand -group Signals -label right_addr /tb_pointer_sel/right_addr
add wave -noupdate -expand -group Signals -label next_addr /tb_pointer_sel/next_addr


run -all

#cosmetic
wave zoom full