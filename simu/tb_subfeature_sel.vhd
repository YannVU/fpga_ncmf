-------------------------------------------------------------------------------
-- Description:
-- Testbench for subfeature_sel
--
--
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY tb_subfeature_sel IS
END ENTITY;

ARCHITECTURE RTL OF tb_subfeature_sel IS

    -- Clock period definitions
    CONSTANT clock_period : TIME := 20 ns;

    --wait duration
    CONSTANT PERIOD : TIME := 0.02 us;

    SIGNAL CLK : STD_LOGIC;
    SIGNAL RST : STD_LOGIC;
    SIGNAL ENDSIM : STD_LOGIC; --simulation end signal

    CONSTANT feature_bitwidth : INTEGER := 8; --feature precision
    CONSTANT subfeature_count : INTEGER := 2; --number of features inside a node
    CONSTANT feature_count : INTEGER := 4; --number of features in the dataset

    SIGNAL example_features : STD_LOGIC_VECTOR ((feature_bitwidth * feature_count) - 1 DOWNTO 0);
    SIGNAL selector : STD_LOGIC_VECTOR (feature_count - 1 DOWNTO 0);
    SIGNAL subfeatures : STD_LOGIC_VECTOR ((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);

BEGIN
    subfeature_sel : ENTITY work.subfeature_sel
        GENERIC MAP(
            feature_bitwidth => feature_bitwidth,
            subfeature_count => subfeature_count,
            feature_count => feature_count
        )
        PORT MAP(
            i_RST => RST,
            i_example_features => example_features,
            i_selector => selector,
            o_subfeatures => subfeatures
        );

    -- clock process
    clock_process : PROCESS
    BEGIN
        IF (ENDSIM = '1') THEN
            WAIT;
        ELSE
            CLK <= '0';
            WAIT FOR clock_period/2;
            CLK <= '1';
            WAIT FOR clock_period/2;
        END IF;
    END PROCESS;

    -- test process
    Test : PROCESS
    BEGIN
        --simulation init start
        RST <= '1';
        ENDSIM <= '0';
        WAIT FOR PERIOD;
        RST <= '0';
        --simulation init end

        --test here
        example_features <= "00000000" & "00000000" & "00000000" & "00000000";
        selector <= "1100";--select first two features
        WAIT FOR PERIOD;

        example_features <= "00000001" & "00000011" & "00000111" & "00001111";
        selector <= "1100";--select features 1 and 2
        WAIT FOR PERIOD;

        selector <= "1010";--select features 1 and 3
        WAIT FOR PERIOD;

        selector <= "0110";--select features 2 and 3
        WAIT FOR PERIOD;

        selector <= "0101";--select features 2 and 4
        WAIT FOR PERIOD;

        selector <= "0011";--select features 3 and 4
        WAIT FOR PERIOD;

        --simulation end

        ENDSIM <= '1';
        WAIT;
    END PROCESS;
END ARCHITECTURE;