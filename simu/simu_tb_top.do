puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/comparator.vhd
vcom -93 ../src/generic_ram_dual_port.vhd
vcom -93 ../src/manhattan.vhd
vcom -93 ../src/node_splitter.vhd
vcom -93 ../src/pointer_sel.vhd
vcom -93 ../src/subfeature_sel.vhd
vcom -93 ../simu/tb_top.vhd

vsim tb_top

add wave -noupdate -expand -group Simulation -label CLK /tb_top/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_top/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_top/ENDSIM

add wave -noupdate -group Constants -label address_bitwidth /tb_top/address_bitwidth
add wave -noupdate -group Constants -label feature_bitwidth /tb_top/feature_bitwidth
add wave -noupdate -group Constants -label subfeature_count /tb_top/subfeature_count
add wave -noupdate -group Constants -label feature_count /tb_top/feature_count
add wave -noupdate -group Constants -label mem_depth /tb_top/mem_depth
add wave -noupdate -group Constants -label data_width /tb_top/data_width
add wave -noupdate -group Constants -label addr_width /tb_top/addr_width
add wave -noupdate -group Constants -label init_file /tb_top/init_file
add wave -noupdate -group Constants -label distance_bitwidth /tb_top/distance_bitwidth

add wave -noupdate -expand -group Signal -label data /tb_top/data
add wave -noupdate -expand -group Signal -label wraddress /tb_top/wraddress
add wave -noupdate -expand -group Signal -label wren /tb_top/wren
add wave -noupdate -expand -group Signal -label rdaddress /tb_top/rdaddress
add wave -noupdate -expand -group Signal -label q /tb_top/q
add wave -noupdate -expand -group Signal -label left_addr /tb_top/left_addr
add wave -noupdate -expand -group Signal -label right_addr /tb_top/right_addr
add wave -noupdate -expand -group Signal -label subfeature_selector /tb_top/subfeature_selector
add wave -noupdate -expand -group Signal -label example_features /tb_top/example_features
add wave -noupdate -expand -group Signal -label subfeatures /tb_top/subfeatures
add wave -noupdate -expand -group Signal -label left_done /tb_top/left_done
add wave -noupdate -expand -group Signal -label left_centroid /tb_top/left_centroid
add wave -noupdate -expand -group Signal -label right_done /tb_top/right_done
add wave -noupdate -expand -group Signal -label right_centroid /tb_top/right_centroid
add wave -noupdate -expand -group Signal -label left_distance /tb_top/left_distance
add wave -noupdate -expand -group Signal -label right_distance /tb_top/right_distance
add wave -noupdate -expand -group Signal -label distance_comparison /tb_top/distance_comparison
add wave -noupdate -expand -group Signal -label next_addr /tb_top/next_addr

run -all

#cosmetic
wave zoom full