puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/comparator.vhd
vcom -93 ../simu/tb_comparator.vhd

vsim tb_comparator

add wave -noupdate -expand -group Simulation -label CLK /tb_comparator/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_comparator/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_comparator/ENDSIM

add wave -noupdate -expand -group Constants -label input_bitwidth /tb_comparator/input_bitwidth

add wave -noupdate -expand -group Signals -label A /tb_comparator/A
add wave -noupdate -expand -group Signals -label B /tb_comparator/B
add wave -noupdate -expand -group Signals -label result /tb_comparator/result

run -all

#cosmetic
wave zoom full