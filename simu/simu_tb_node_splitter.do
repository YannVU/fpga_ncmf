puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/node_splitter.vhd
vcom -93 ../simu/tb_node_splitter.vhd

vsim tb_node_splitter

add wave -noupdate -expand -group Simulation -label CLK /tb_node_splitter/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_node_splitter/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_node_splitter/ENDSIM

add wave -noupdate -expand -group Constants -label address_bitwidth /tb_node_splitter/address_bitwidth
add wave -noupdate -expand -group Constants -label feature_bitwidth /tb_node_splitter/feature_bitwidth
add wave -noupdate -expand -group Constants -label subfeature_count /tb_node_splitter/subfeature_count
add wave -noupdate -expand -group Constants -label feature_count /tb_node_splitter/feature_count
add wave -noupdate -expand -group Constants -label offset0 /tb_node_splitter/node_splitter/offset0
add wave -noupdate -expand -group Constants -label offset1 /tb_node_splitter/node_splitter/offset1
add wave -noupdate -expand -group Constants -label offset2 /tb_node_splitter/node_splitter/offset2
add wave -noupdate -expand -group Constants -label offset3 /tb_node_splitter/node_splitter/offset3

add wave -noupdate -expand -group Signals -label node_info /tb_node_splitter/node_info
add wave -noupdate -expand -group Signals -label left_addr /tb_node_splitter/left_addr
add wave -noupdate -expand -group Signals -label right_addr /tb_node_splitter/right_addr
add wave -noupdate -expand -group Signals -label centroid /tb_node_splitter/centroid
add wave -noupdate -expand -group Signals -label subfeature_sel /tb_node_splitter/subfeature_sel

run -all

#cosmetic
wave zoom full