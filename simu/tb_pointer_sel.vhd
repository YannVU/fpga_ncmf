-------------------------------------------------------------------------------
-- Description:
-- Testbench for subfeature_sel
--
--
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY tb_pointer_sel IS
END ENTITY;

ARCHITECTURE RTL OF tb_pointer_sel IS

    -- Clock period definitions
    CONSTANT clock_period : TIME := 20 ns;

    --wait duration
    CONSTANT PERIOD : TIME := 0.02 us;

    SIGNAL CLK : STD_LOGIC;
    SIGNAL RST : STD_LOGIC;
    SIGNAL ENDSIM : STD_LOGIC; --simulation end signal

    CONSTANT address_bitwidth : INTEGER := 6; --feature precision

    SIGNAL selector : STD_LOGIC;
    SIGNAL left_addr : STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
    SIGNAL right_addr : STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
    SIGNAL next_addr : STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);


BEGIN
    pointer_sel : ENTITY work.pointer_sel
        GENERIC MAP(
            address_bitwidth => address_bitwidth
        )
        PORT MAP(
            i_selector => selector,
            i_left_addr => left_addr,
            i_right_addr => right_addr,
            o_next_addr => next_addr
        );

    -- clock process
    clock_process : PROCESS
    BEGIN
        IF (ENDSIM = '1') THEN
            WAIT;
        ELSE
            CLK <= '0';
            WAIT FOR clock_period/2;
            CLK <= '1';
            WAIT FOR clock_period/2;
        END IF;
    END PROCESS;

    -- test process
    Test : PROCESS
    BEGIN
        --simulation init start
        RST <= '1';
        ENDSIM <= '0';
        WAIT FOR PERIOD;
        RST <= '0';
        --simulation init end

        --test here
        selector <= '0';
        right_addr <= "000111";
        left_addr <= "111000";
        WAIT FOR PERIOD;

        selector <= '1';
        right_addr <= "000111";
        left_addr <= "111000";
        WAIT FOR PERIOD;

        selector <= '0';
        right_addr <= "000001";
        left_addr <= "100000";
        WAIT FOR PERIOD;

        selector <= '1';
        right_addr <= "000001";
        left_addr <= "100000";
        WAIT FOR PERIOD;

        --simulation end

        ENDSIM <= '1';
        WAIT;
    END PROCESS;
END ARCHITECTURE;