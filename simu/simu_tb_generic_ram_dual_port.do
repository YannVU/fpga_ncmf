puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/generic_ram_dual_port.vhd
vcom -93 ../simu/tb_generic_ram_dual_port.vhd

vsim tb_generic_ram_dual_port

add wave -noupdate -expand -group Simulation -label CLK /tb_generic_ram_dual_port/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_generic_ram_dual_port/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_generic_ram_dual_port/ENDSIM

add wave -noupdate -expand -group Constants -label data_width /tb_generic_ram_dual_port/data_width
add wave -noupdate -expand -group Constants -label addr_width /tb_generic_ram_dual_port/addr_width
add wave -noupdate -expand -group Constants -label init_file /tb_generic_ram_dual_port/init_file

add wave -noupdate -expand -group Signals -label data /tb_generic_ram_dual_port/data
add wave -noupdate -expand -group Signals -label rdaddress -radix unsigned /tb_generic_ram_dual_port/rdaddress
add wave -noupdate -expand -group Signals -label wraddress /tb_generic_ram_dual_port/wraddress
add wave -noupdate -expand -group Signals -label wren /tb_generic_ram_dual_port/wren
add wave -noupdate -expand -group Signals -label q -radix unsigned /tb_generic_ram_dual_port/q


run -all

#cosmetic
wave zoom full