-------------------------------------------------------------------------------
-- Description:
-- Testbench for comparator
--
--
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY tb_comparator IS
END ENTITY;

ARCHITECTURE RTL OF tb_comparator IS

    -- Clock period definitions
    CONSTANT clock_period : TIME := 20 ns;

    --wait duration
    CONSTANT PERIOD : TIME := 0.02 us;

    SIGNAL CLK : STD_LOGIC;
    SIGNAL RST : STD_LOGIC;
    SIGNAL ENDSIM : STD_LOGIC; --simulation end signal

    CONSTANT input_bitwidth : INTEGER := 8;

    SIGNAL A : STD_LOGIC_VECTOR(input_bitwidth - 1 DOWNTO 0);
    SIGNAL B : STD_LOGIC_VECTOR(input_bitwidth - 1 DOWNTO 0);
    SIGNAL result : STD_LOGIC;

BEGIN
    comparator : ENTITY work.comparator
        GENERIC MAP(
            input_bitwidth => input_bitwidth
        )
        PORT MAP(
            i_A => A,
            i_B => B,
            o_result => result
        );

    -- clock process
    clock_process : PROCESS
    BEGIN
        IF (ENDSIM = '1') THEN
            WAIT;
        ELSE
            CLK <= '0';
            WAIT FOR clock_period/2;
            CLK <= '1';
            WAIT FOR clock_period/2;
        END IF;
    END PROCESS;

    -- test process
    Test : PROCESS
    BEGIN
        --simulation init start
        RST <= '1';
        ENDSIM <= '0';
        WAIT FOR PERIOD;
        RST <= '0';
        --simulation init end

        --test here
        A <= "00000001";
        B <= "00000001";
        WAIT FOR PERIOD;

        A <= "00000001";
        B <= "10000000";
        WAIT FOR PERIOD;

        A <= "10000000";
        B <= "00000001";
        WAIT FOR PERIOD;

        --simulation end
        ENDSIM <= '1';
        WAIT;
    END PROCESS;
END ARCHITECTURE;