-------------------------------------------------------------------------------
-- Description:
-- Testbench for node_splitter
--
--
-------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

use IEEE.math_real.ALL;
--use IEEE.math_real."ceil";
--use IEEE.math_real."log2";

ENTITY tb_manhattan_old IS
END ENTITY;

ARCHITECTURE RTL OF tb_manhattan_old IS

    -- Clock period definitions
    CONSTANT clock_period : TIME := 20 ns;

    --wait duration
    CONSTANT PERIOD : TIME := 0.02 us;

    SIGNAL CLK : STD_LOGIC;
    SIGNAL RST : STD_LOGIC;
    SIGNAL ENDSIM : STD_LOGIC; --simulation end signal

    CONSTANT feature_bitwidth : INTEGER := 8; --feature precision
    CONSTANT subfeature_count : INTEGER := 3; --number of features inside a node

    SIGNAL A : STD_LOGIC_VECTOR((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);
    SIGNAL B : STD_LOGIC_VECTOR((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);

    SIGNAL distance : integer;
    SIGNAL done : STD_LOGIC;

BEGIN
    manhattan_old : ENTITY work.manhattan_old
        GENERIC MAP(
            N => feature_bitwidth,
            M => subfeature_count
        )
        PORT MAP(
            rst => RST,
            A => A,
            B => B,
            Distance => distance,
            done => done
        );

    -- clock process
    clock_process : PROCESS
    BEGIN
        IF (ENDSIM = '1') THEN
            WAIT;
        ELSE
            CLK <= '0';
            WAIT FOR clock_period/2;
            CLK <= '1';
            WAIT FOR clock_period/2;
        END IF;
    END PROCESS;

    -- test process
    Test : PROCESS
    BEGIN
        --simulation init start
        RST <= '1';
        ENDSIM <= '0';
        WAIT FOR PERIOD;
        RST <= '0';
        --simulation init end

        --test here
        A <= "01111111" & "01111111" & "01111111";
        B <= "00000000" & "00000000" & "00000000";
        RST <= '1';
        WAIT FOR PERIOD;
        RST <= '0';
        WAIT FOR 100*PERIOD;

        A <= "00000001" & "00000001" & "00000001";
        B <= "00000000" & "00000000" & "00000000";
        RST <= '1';
        WAIT FOR PERIOD;
        RST <= '0';
        WAIT FOR 100*PERIOD;

        A <= "00000000" & "11111111" & "11111111";
        B <= "00000000" & "00000000" & "00000000";
        RST <= '1';
        WAIT FOR PERIOD;
        RST <= '0';
        WAIT FOR 100*PERIOD;

        A <= "00000000" & "00000000" & "11111111";
        B <= "00000000" & "00000000" & "00000000";
        RST <= '1';
        WAIT FOR PERIOD;
        RST <= '0';
        WAIT FOR 100*PERIOD;

        A <= "00000000" & "00000000" & "00000000";
        B <= "00000000" & "00000000" & "00000000";
        RST <= '1';
        WAIT FOR PERIOD;
        RST <= '0';
        WAIT FOR 100*PERIOD;


        --simulation end
        ENDSIM <= '1';
        WAIT;
    END PROCESS;
END ARCHITECTURE;