puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/manhattan.vhd
vcom -93 ../simu/tb_manhattan.vhd

vsim tb_manhattan

add wave -noupdate -expand -group Simulation -label CLK /tb_manhattan/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_manhattan/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_manhattan/ENDSIM

add wave -noupdate -expand -group Constants -label feature_bitwidth /tb_manhattan/feature_bitwidth
add wave -noupdate -expand -group Constants -label subfeature_count /tb_manhattan/subfeature_count

add wave -noupdate -expand -group Signals -label A /tb_manhattan/A
add wave -noupdate -expand -group Signals -label B /tb_manhattan/B
add wave -noupdate -expand -group Signals -label distance /tb_manhattan/distance
add wave -noupdate -expand -group Signals -label done /tb_manhattan/done

run -all

#cosmetic
wave zoom full