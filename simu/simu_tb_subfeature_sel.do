puts "Simulation script for ModelSim "

vlib work
vcom -93 ../src/subfeature_sel.vhd
vcom -93 ../simu/tb_subfeature_sel.vhd

vsim tb_subfeature_sel

add wave -noupdate -expand -group Simulation -label CLK /tb_subfeature_sel/CLK
add wave -noupdate -expand -group Simulation -label RST /tb_subfeature_sel/RST
add wave -noupdate -expand -group Simulation -label ENDSIM /tb_subfeature_sel/ENDSIM

add wave -noupdate -expand -group Constants -label feature_bitwidth /tb_subfeature_sel/feature_bitwidth
add wave -noupdate -expand -group Constants -label subfeature_count /tb_subfeature_sel/subfeature_count
add wave -noupdate -expand -group Constants -label feature_count /tb_subfeature_sel/feature_count

add wave -noupdate -expand -group Signals -label example_features /tb_subfeature_sel/example_features
add wave -noupdate -expand -group Signals -label selector /tb_subfeature_sel/selector
add wave -noupdate -expand -group Signals -label subfeatures /tb_subfeature_sel/subfeatures

run -all

#cosmetic
wave zoom full