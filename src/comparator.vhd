LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY comparator IS
    GENERIC (
        input_bitwidth : INTEGER := 8
    );
    PORT (
        i_A : IN STD_LOGIC_VECTOR(input_bitwidth - 1 DOWNTO 0);
        i_B : IN STD_LOGIC_VECTOR(input_bitwidth - 1 DOWNTO 0);

        o_result : OUT STD_LOGIC --high if A is bigger than B, else low
    );
END ENTITY;

ARCHITECTURE rtl OF comparator IS

BEGIN
    PROCESS (i_A, i_B)
    BEGIN
        IF i_A > i_B THEN
            o_result <= '1';
        ELSE
            o_result <= '0';
        END IF;
    END PROCESS;
END ARCHITECTURE;