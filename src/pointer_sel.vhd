LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
ENTITY pointer_sel IS
    GENERIC (
        address_bitwidth : INTEGER := 6
    );
    PORT (
        i_selector : IN STD_LOGIC;
        i_left_addr : IN STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
        i_right_addr : IN STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
        o_next_addr : OUT STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0)
    );
END ENTITY;

ARCHITECTURE rtl OF pointer_sel IS

BEGIN

    PROCESS (i_selector, i_left_addr, i_right_addr) IS
    BEGIN
        CASE i_selector IS
            WHEN '0' =>
                o_next_addr <= i_left_addr;
            WHEN OTHERS =>
                o_next_addr <= i_right_addr;
        END CASE;
    END PROCESS;
END ARCHITECTURE;