LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;


ENTITY subfeature_sel IS
    GENERIC (
        feature_bitwidth : INTEGER := 8; --feature precision
        subfeature_count : INTEGER := 2; --number of features inside a node
        feature_count : INTEGER := 4 --number of features in the examples
    );
    PORT (
        i_RST : IN STD_LOGIC;
        i_example_features : IN STD_LOGIC_VECTOR ((feature_bitwidth * feature_count) - 1 DOWNTO 0);
        i_selector : IN STD_LOGIC_VECTOR (feature_count - 1 DOWNTO 0);
        o_subfeatures : OUT STD_LOGIC_VECTOR ((feature_bitwidth * subfeature_count) - 1 DOWNTO 0)
    );
END ENTITY;

ARCHITECTURE rtl OF subfeature_sel IS

BEGIN
    PROCESS (i_example_features, i_selector) --the process of calculation can only be launched by using rst command after connected some values to A and B
        VARIABLE sub_offset : NATURAL RANGE 0 TO subfeature_count * (feature_bitwidth - 1);
        VARIABLE ex_offset : NATURAL RANGE 0 TO feature_count * (feature_bitwidth - 1);
        VARIABLE count : NATURAL RANGE 0 TO feature_count + 2;

    BEGIN
        --REPORT "start";
        count := 0;

        FOR i IN 0 TO subfeature_count - 1 LOOP
            --REPORT "i :" & INTEGER'image(i);
            WHILE count <= feature_count - 1 LOOP
                IF i_selector(count) = '1' THEN

                    --REPORT "count :" & INTEGER'image(count);
                    sub_offset := feature_bitwidth * i;
                    ex_offset := feature_bitwidth * count;
                    
                    o_subfeatures(sub_offset + feature_bitwidth - 1 DOWNTO sub_offset) <= i_example_features(ex_offset + feature_bitwidth - 1 DOWNTO ex_offset);
                    EXIT;
                END IF;
                count := count + 1;
            END LOOP;

            count := count + 1;
        END LOOP;
        --REPORT "end";
    END PROCESS;

END ARCHITECTURE;