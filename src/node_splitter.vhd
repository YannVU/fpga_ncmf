LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY node_splitter IS
    GENERIC (
        address_bitwidth : INTEGER := 6;
        feature_bitwidth : INTEGER := 8; --feature precision
        subfeature_count : INTEGER := 2; --number of features inside a node
        feature_count : INTEGER := 4 --number of features in the examples
    );
    PORT (
        i_node_info : IN STD_LOGIC_VECTOR (0 TO (address_bitwidth * 2) + (feature_bitwidth * subfeature_count * 2) + feature_count - 1 );
        o_left_addr : OUT STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
        o_right_addr : OUT STD_LOGIC_VECTOR (address_bitwidth - 1 DOWNTO 0);
        o_left_centroid : OUT STD_LOGIC_VECTOR ((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);
        o_right_centroid : OUT STD_LOGIC_VECTOR ((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);
        o_subfeature_sel : OUT STD_LOGIC_VECTOR (feature_count - 1 DOWNTO 0)
    );
END ENTITY;

ARCHITECTURE rtl OF node_splitter IS
    --couldn't find a simpler way to offset ranges
    constant offset0 : natural := 0;
    subtype left_addr_range is natural range offset0 TO offset0 +o_left_addr'length - 1;
    constant offset1 : natural := offset0 + o_left_addr'length;
    subtype right_addr_range is natural range offset1 TO offset1 + o_right_addr'length - 1;
    constant offset2 : natural := offset1 + o_right_addr'length;
    subtype left_centroid_range is natural range offset2 TO offset2 + o_left_centroid'length - 1;
    constant offset3 : natural := offset2 + o_left_centroid'length;
    subtype right_centroid_range is natural range offset3 TO offset3 + o_right_centroid'length - 1;
    constant offset4 : natural := offset3 + o_right_centroid'length;
    subtype subfeature_sel_range is natural range offset4 TO offset4 + o_subfeature_sel'length - 1;

BEGIN
    --split vector into desired outputs
    o_left_addr <= i_node_info(left_addr_range);
    o_right_addr <= i_node_info(right_addr_range);
    o_left_centroid <= i_node_info(left_centroid_range);
    o_right_centroid <= i_node_info(right_centroid_range);
    o_subfeature_sel <= i_node_info(subfeature_sel_range);
END ARCHITECTURE;