LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

USE IEEE.math_real.ALL;
--use IEEE.math_real."ceil";
--use IEEE.math_real."log2";

ENTITY manhattan IS
    GENERIC (
        feature_bitwidth : INTEGER := 8; --default precision : 8 bits per feature
        subfeature_count : INTEGER := 5 --default nb of features : 5
    );
    PORT (
        i_RST : IN STD_LOGIC;
        i_A : IN STD_LOGIC_VECTOR((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);
        i_B : IN STD_LOGIC_VECTOR((feature_bitwidth * subfeature_count) - 1 DOWNTO 0);

        o_distance : OUT STD_LOGIC_VECTOR (INTEGER(ceil(log2(real((2 ** feature_bitwidth - 1) * subfeature_count)))) - 1 DOWNTO 0); --should be able to fit the maximum sum of all vectors
        o_done : OUT STD_LOGIC
    );
END ENTITY;

ARCHITECTURE rtl OF manhattan IS
    
BEGIN
    PROCESS (i_RST, i_A, i_B) --the process of calculation can only be launched by using i_RST command after connected some values to A and B
        VARIABLE TMP : UNSIGNED(o_distance'length - 1 DOWNTO 0);
        VARIABLE feature_A, feature_B : UNSIGNED(feature_bitwidth - 1 DOWNTO 0);
        VARIABLE offset : INTEGER RANGE 0 TO subfeature_count * (feature_bitwidth - 1);

    BEGIN
        IF i_RST = '1' THEN
            o_done <= '0';
            
        ELSE
            TMP := (others => '0');
            FOR i IN 0 TO subfeature_count - 1 LOOP
                --loop for very subfeature : 
                offset := i * feature_bitwidth; --get subfeature offset

                feature_A := UNSIGNED(i_A(offset + feature_bitwidth - 1 DOWNTO offset)); --we take the values of the feature for each vector
                feature_B := UNSIGNED(i_B(offset + feature_bitwidth - 1 DOWNTO offset));

                IF (feature_A > feature_B) THEN -- avoid negative unsigned shenanighans
                    TMP := UNSIGNED(TMP) + (feature_A - feature_B);
                ELSE
                    TMP := UNSIGNED(TMP) + (feature_B - feature_A);
                END IF;

            END LOOP;
            o_done <= '1';
            o_distance <= STD_LOGIC_VECTOR(TMP);
        END IF;
    END PROCESS;
    
END ARCHITECTURE;