-------------------------------------------------------------------------------
-- Description:
--  generic function that return the value of the Manhattan distance between
--  two given vectors. 
--
-- Generic : 
-- M is the number of features (dimentions) in each vector
-- N is the number of bits for each vector (precision)
--
-- ex : vector = [ft1 ft2 ... ftm]
--      feature = bit1 bit2 ... bitn
--
-- nb1 : --  For the moment, we consider that the value ft1,S ... are integers. 
-- nb2 : in reality we begin to count from 0 so we have ft0 ft1 ... ft(m-1) and
--      bit0 bit1 ... bit(n-1)
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity manhattan_old is
    generic(
        N : integer := 8; --default precision : 8 bits per feature
        M : integer := 5 --default nb of features : 5
    );
    port(
        rst : in std_logic;
        A : in std_logic_vector((N*M)-1 downto 0);  
        B : in std_logic_vector((N*M)-1 downto 0);

        Distance : out integer := 0;
        done : out std_logic --allow to know when the calculation is finished
    );
end entity;


architecture RTL of manhattan_old is    

begin

    process(rst, A, B) --the process of calculation can only be launched by using rst command after connected some values to A and B

        variable TMP,TMP1 : integer;
        variable i : integer := 0;
        variable extremity1, extremity2 : integer := 0;
        variable intA, intB : integer;

    begin
        if rst = '1' then
            i := 0; 
            done <= '0';
            Distance <= 0;
            TMP := 0; 
        else
            for i in 0 to M-1 loop
            --we do the calculation abs(ftiA - ftiB) for every feature (dimension)
                extremity1 := i*N;            --we compute the extremities of the feature
                extremity2 := ((i+1)*N)-1;
                
                intA := to_integer(signed(A(extremity2 downto extremity1)));   --we take the values of the feature for each vector
                intB := to_integer(signed(B(extremity2 downto extremity1)));

                TMP := TMP + abs(intA - intB); --we compute the manhattan distance for this feature (dimension) and add it to the global manhattan distance
    
            end loop;
            done <= '1';
        end if;
    
        Distance <= TMP; --we affect the final value to the output
        
    
    end process;
end architecture;
