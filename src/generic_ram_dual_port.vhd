LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY altera_mf;
USE altera_mf.altera_mf_components.ALL;

ENTITY generic_ram_dual_port IS
    GENERIC (
        data_width : INTEGER := 8;
        addr_width : INTEGER := 4;
        init_file : STRING := "../src/testos.mif"
    );
    PORT (
        clock : IN STD_LOGIC := '1';
        data : IN STD_LOGIC_VECTOR (data_width - 1 DOWNTO 0);
        rdaddress : IN STD_LOGIC_VECTOR (addr_width - 1 DOWNTO 0);
        wraddress : IN STD_LOGIC_VECTOR (addr_width - 1 DOWNTO 0);
        wren : IN STD_LOGIC := '0';
        q : OUT STD_LOGIC_VECTOR (data_width - 1 DOWNTO 0)
    );
END generic_ram_dual_port;

ARCHITECTURE SYN OF generic_ram_dual_port IS

    SIGNAL sub_wire0 : STD_LOGIC_VECTOR (data_width - 1 DOWNTO 0);
    CONSTANT numwords : INTEGER := 2 ** addr_width;

BEGIN
    q <= sub_wire0(data_width - 1 DOWNTO 0);

    altsyncram_component : altsyncram
    GENERIC MAP(
        address_aclr_b => "NONE",
        address_reg_b => "CLOCK0",
        clock_enable_input_a => "BYPASS",
        clock_enable_input_b => "BYPASS",
        clock_enable_output_b => "BYPASS",
        init_file => init_file,
        intended_device_family => "Cyclone V",
        lpm_type => "altsyncram",
        numwords_a => numwords,
        numwords_b => numwords,
        operation_mode => "DUAL_PORT",
        outdata_aclr_b => "NONE",
        outdata_reg_b => "CLOCK0",
        power_up_uninitialized => "FALSE",
        read_during_write_mode_mixed_ports => "DONT_CARE",
        widthad_a => addr_width,
        widthad_b => addr_width,
        width_a => data_width,
        width_b => data_width,
        width_byteena_a => 1
    )
    PORT MAP(
        address_a => wraddress,
        address_b => rdaddress,
        clock0 => clock,
        data_a => data,
        wren_a => wren,
        q_b => sub_wire0
    );

END SYN;